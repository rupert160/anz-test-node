var expect    = require("chai").expect;
var app = require("../server.js");
        
describe("API Tester", function() {
    describe("API Response Code Test", function() {
        it("Returns a 2XX code eg 201 or 200", function() {
            var request = require('request-promise');

            var options = {
              url: 'http://localhost:3000/status',
              headers: {'content-type': 'application/json'},
              resolveWithFullResponse: true
            };
            
            request(options)
                .then(function(response){ expect(response.statusCode).to.equal(200) })
                .catch(function(err){console.log(err)})
        });
    });
});