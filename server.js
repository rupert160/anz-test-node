const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => res.send('Hello World!'))
app.listen(port, () => console.log(`Example app listening on port ${port}!`))

app.get('/status', (req, res) => {
  //easier to be a Syncronous child process
  const spawn = require('child_process').spawnSync;

  const proc1 = spawn('git', ['rev-parse','HEAD']);
  let sha = proc1.stdout.slice(0, -1)
  
  const proc2 = spawn('git', ['describe','--abbrev=']);
  let tag = proc2.stdout.slice(0,-1)
  
  return res.send("{\"version\": \"" +
   tag + 
   "\", \"lastcommitsha\": \"" + 
   sha + 
   "\", \"description\": \"pre-interview technical test /status \"}");
 });