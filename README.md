# Preparation for interview

- Staged Builds
- Node.js API with git sha and version response
- Testing and devops cycle documentation

# Installation - tested on Fedora 30 and Ubuntu 16.04 LTS x86 64bit

- Install Git and Git-LFS


https://git-lfs.github.com/


## Install Docker
- on Fedora 30
```
sudo dnf config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo
sudo dnf makecache
sudo dnf install docker-ce docker-ce-cli containerd.io
sudo systemctl enable docker
sudo systemctl start docker
sudo usermod -aG docker ${USER}
```
- on Ubuntu 16.04

```
sudo apt update
sudo apt install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt update
sudo apt install docker-ce docker-ce-cli containerd.io
sudo systemctl enable docker
sudo systemctl start docker
sudo usermod -aG docker ${USER}
```
    
## Install docker-compose (either Fedora 30 or Ubuntu 16.04)

```
sudo curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" \
    -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```

## Install Visual Studio Code

There are a few options, so best to view the vscode [installation docs]: https://code.visualstudio.com/docs/setup/linux
I prefer to add the repositories to my OS's for a persistant review of all my active repos

## Clone the repository with GIT

* Within VSCode, Press **F1** and type `Git: Clone` and paste `https://bitbucket.org/rupert-d2i/anz-test-node` Navigate to a directory that you'd like to place this program locally within, thence select Open Here.
![picture](images/git_clone.png)
![picture](images/git_clone_url.png)
![picture](images/select_location.png)

* Within VSCode press **ctrl** + **shift** + **X** and type Remote Development (this extension allows connection to Windows Subsystem for Linux, SSH to machines and Mounting/Spawing containers for native Linux development) Press Install for this extension to permit Building within a container.
![picture](images/install_remote_devel.png)

* Within VSCode, Press **F1** and type `Remote-Container: Open folder in container...` re-navigate to your project directory root, This will then run the docker-compose.yml with a build statement, which will build/rebuild your container and inject your project files.
![picture](images/open_in_container.png)
![picture](images/running_docker_compose.png)
![picture](images/finished_debug.png)

## Run Debugging within Container.

* Once comleted, press **ctrl** + **shift** + **D** to get to the Debug screen. 
* At the top you will see "Debug" and a green arrow, with a drop-down.
* Select "Mocha tests" in drop-down, thence press the Green "Run" arrow.
* When (Successful) test is run, kill debugging with **Shift** + **F5**
